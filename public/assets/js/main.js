
// Config
const   logoSize = 50,
        bigFont = 'bold 36pt Verdana,Geneva,sans-serif',
        smallFont = 'bold 8pt Verdana,Geneva,sans-serif',
        textAlign = 'center',
        textBaseline = 'middle',
        fillStyle = 'white',
        shadowColor = 'rgb(0,255,255)',
        shadowBlur = 15,
        lineHeight = 40,
        logoText = 'unity4j.com',
        leftBranding = '#Unity4J',
        logoTextOffsetX = Math.round(logoSize / 4),
        logoTextOffsetY = Math.round(logoSize / 2),
        maxWidth = 500,
        maxHeight = 500,
        aspectRatio = 1,
        imgurAlbumId = 'd2Zibek',
        imgurAlbumHash = 'JId5Bn1BiHC5L5y',
        galleryImageAmount = 100
        ;

// Dom elements
const   dropArea = document.getElementById('dropArea'),
        logo = document.getElementById('unity4j-logo-small'),
        crop = document.getElementById('cropping'),
        caption = document.getElementById('caption'),
        topCaption = document.getElementById('top-caption'),
        bottomCaption = document.getElementById('bottom-caption'),
        imgurGallery = document.getElementById('imgurGallery')
        ;

// globals
let     cropInstance,
        sourceImage,
        context,
        topBaseline,
        bottomBaseline,
        center,
        cropArea,
        canvas
        ;


// Fileupload
// props to https://www.smashingmagazine.com/2018/01/drag-drop-file-uploader-vanilla-js/
dropArea.addEventListener('dragenter', highlight, false);
dropArea.addEventListener('dragover', highlight, false);
dropArea.addEventListener('dragleave', unhighlight, false);
dropArea.addEventListener('drop', unhighlight, false);

function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
}

function highlight(e) {
    preventDefaults(e);
    dropArea.classList.add('highlight');
}

function unhighlight(e) {
    preventDefaults(e);
    dropArea.classList.remove('highlight');
}

dropArea.addEventListener('drop', handleDrop, false);

function handleDrop(e) {
      let dt = e.dataTransfer;
      let files = dt.files;

      handleFiles(files);
}

function handleFiles(files) {
      processFile(files[0]);
}

function processFile(file) {
    // props to https://blueimp.github.io/JavaScript-Load-Image/
    var loadingImage = loadImage(
        file,
        function (fixedOrientation) {
            sourceImage = document.createElement('img');
            sourceImage.src = fixedOrientation.toDataURL();

            while (crop.firstChild) {
                crop.removeChild(crop.firstChild);
            }

            // props to https://jamesooi.design/Croppr.js/
            cropInstance = new Croppr(sourceImage, {
                aspectRatio: aspectRatio
            });

            crop.appendChild(sourceImage);

            document.getElementById('crop').scrollIntoView();
        },
        {orientation: true}
    );
}

// Cropping
function processCrop() {
    canvas = document.createElement('canvas');
    context = canvas.getContext('2d');

    cropArea = cropInstance.getValue();

    canvas.width = cropArea.width > maxWidth ? maxWidth : cropArea.width;
    canvas.height = cropArea.height > maxHeight ? maxHeight : cropArea.height;

    const tiles = canvas.height / 9;
    topBaseline = tiles * 1;
    bottomBaseline = tiles * 7;

    center = canvas.width / 2;


    updateGraphics();

    while (caption.firstChild) {
        caption.removeChild(caption.firstChild);
    }

    caption.appendChild(canvas);

    document.getElementById('writing').scrollIntoView();
}

// Captions
function updateGraphics() {
    context.textAlign = textAlign;
    context.textBaseline = textBaseline;
    context.fillStyle = fillStyle;
    context.shadowBlur = 0;
    context.font = smallFont;
    context.save();

    context.drawImage(sourceImage, cropArea.x, cropArea.y, cropArea.width, cropArea.height, 
        0, 0, canvas.width, canvas.height);

    context.shadowColor = shadowColor;
    context.shadowBlur = shadowBlur;
    context.font = bigFont;
    context.save();

    wrapText(topCaption.value, center, topBaseline);
    wrapText(bottomCaption.value, center, bottomBaseline);

    context.font = smallFont;
    context.textAlign = 'right';
    context.save();

    context.fillText(logoText, canvas.width - (logoSize + logoTextOffsetX), canvas.height - logoTextOffsetY);

    context.textAlign = 'left';
    context.save();

    context.fillText(leftBranding, logoTextOffsetX, logoTextOffsetY);

    context.shadowBlur = 0;
    context.save();

    context.drawImage(logo, canvas.width - logoSize, canvas.height - logoSize, logoSize, logoSize);
}

// props to https://www.html5canvastutorials.com/tutorials/html5-canvas-wrap-text-tutorial/
function wrapText(text, x, y) {
    text = text.trim();
    const words = text.split(' ');
    let line = '';

    for(let n = 0; n < words.length; n++) {
        const testLine = line + words[n];
        const metrics = context.measureText(testLine);
        const testWidth = metrics.width;

        if (testWidth > canvas.width && n > 0) {
            line = line.substring(0, line.length - 1);
            context.fillText(line, x, y);
            line = words[n] + ' ';
            y += lineHeight;
        } else {
            line = n < (words.length - 1) ? testLine + ' ' : testLine;
        }
    }

    context.fillText(line, x, y);
}
	
function download() {
    const download = document.getElementById('download');
    const image = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream');
    download.setAttribute('href', image);
    download.setAttribute('download','my-meme.png');
}

function openInTab() {
    window.open(canvas.toDataURL(), '_blank');
}


function uploadToImgur() {
    fetch('https://api.imgur.com/3/image', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json',
            'Authorization': 'Client-ID 5d89824f2a750e7'
        },
        body: JSON.stringify({
            image: canvas.toDataURL("image/png").split(',')[1],
            album: imgurAlbumHash
        })
    })
    .then(raw => raw.json())
    .then(response => {
        document.getElementById('gallery').scrollIntoView();
        loadImgurGallery(response.data.id,response.data.link);
    })
    .catch(error => {
        console.log(error)
    });
};

function loadImgurGallery(id, link) {
    const apiUrl = 'https://api.imgur.com/3/album/'+imgurAlbumId+'/images';

    fetch(apiUrl, {
        method: 'GET',
        headers: {
            'Authorization': 'Client-ID 5d89824f2a750e7'
        },
    })
    .then(raw => raw.json())
    .then(response => {
        prepareGallery(response.data, id, link)
    })
    .catch(error => {
        console.log(error)
    });
}

function prepareGallery(imgurPics, id, link) {
    imgurPics.sort((a,b) => a.datetime > b.datetime ? -1 : b.datetime > a.datetime ? 1 : 0);

    while (imgurGallery.firstChild) {
        imgurGallery.removeChild(imgurGallery.firstChild);
    }

    if (id !== undefined && link !== undefined) {
        addPictureToGallery(id, link);
    }

    totalMaxImages = imgurPics.length < galleryImageAmount ? imgurPics.length : galleryImageAmount;

    for (let i = 0; i < totalMaxImages; i++) {
        addPictureToGallery(imgurPics[i].id,imgurPics[i].link)
    }
}

function addPictureToGallery(id, link) {
    const wrapperAnchor = document.createElement('a');
    wrapperAnchor.target = '_blank';
    wrapperAnchor.href = 'https://imgur.com/' + id;

    const galleryImage = document.createElement('img');
    galleryImage.src = link;
    galleryImage.classList.add = 'imgurImage';

    wrapperAnchor.appendChild(galleryImage);

    imgurGallery.appendChild(wrapperAnchor);
}


const galleryUrl = 'https://imgur.com/a/' + imgurAlbumId;
const imgurGalleryLink = document.getElementById('imgurGalleryLink');
imgurGalleryLink.href = galleryUrl;

loadImgurGallery();




















